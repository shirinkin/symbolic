﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

namespace TestApplication
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        Symbolic.Functions.CommonF function;

        private void button_Click(object sender, RoutedEventArgs e)
        {
            String source = sourceFunc.Text;
            function = Symbolic.Parser.ParseExpressionObject(source, null);
            String resultS = function.Print();
            resultFunc.Text = resultS;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            String argumentsS = arguments.Text;
            int index = argumentsS.IndexOf(';');
            int oldIndex = -1;
            List<double> argList = new List<double>();
            while (index != -1)
            {
                //argList.Add(Double.Parse(argumentsS.Substring(oldIndex + 1, index - oldIndex - 1), CultureInfo.InvariantCulture.NumberFormat));
                argList.Add(Symbolic.Parser.ParseExpression(argumentsS.Substring(oldIndex + 1, index - oldIndex - 1), null)(null));
                oldIndex = index;
                index = argumentsS.IndexOf(';', oldIndex + 1);
            }
            //argList.Add(Double.Parse(argumentsS.Substring(oldIndex + 1), CultureInfo.InvariantCulture.NumberFormat));
            argList.Add(Symbolic.Parser.ParseExpression(argumentsS.Substring(oldIndex + 1), null)(null));
            double[] args = argList.ToArray();
            value.Text = "" + function.Invoke(args);
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            var derText = Symbolic.Derivative.GetDerivative(resultFunc.Text, derArg.Text, false);
            derBox.Text = Symbolic.Parser.Optimize(derText);
        }
    }
}
