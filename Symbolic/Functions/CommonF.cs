﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Symbolic.Functions
{
    /// <summary>
    /// Общий вид действительной функции многих переменных. База для всех классов.
    /// </summary>
    public abstract class CommonF : IFunction
    {
        /// <summary>
        /// Оператор сложения.
        /// </summary>
        /// <param name="left">левый операнд</param>
        /// <param name="right">правый операнд</param>
        /// <returns>Новый объект типа Sum</returns>
        public static Sum operator +(CommonF left, CommonF right)
        {
            //новый объект
            Sum result = new Sum();
            //если левый операнд представляет собой сумму других функций
            if (left is Sum)
            {
                //добавляем эти функции в нашу сумму
                result.operands = ((Sum)left).operands;
                result.signs = ((Sum)left).signs;
            }
            else
            {
                //если нет, то добавляем в нашу сумму саму функцию
                result.operands.Add(left);
                result.signs.Add(true);
            }
            //то же самое для правого операнда
            if (right is Sum)
            {
                result.operands.AddRange(((Sum)right).operands);
                result.signs.AddRange(((Sum)right).signs);
            }
            else
            {
                result.operands.Add(right);
                result.signs.Add(true);
            }
            return result;
        }

        public static CommonF operator +(CommonF f)
        {
            return f.Clone();
        }

        /// <summary>
        /// Оператор вычитания. 
        /// </summary>
        /// <param name="left">левый операнд</param>
        /// <param name="right">правый операнд</param>
        /// <returns>Новый объект типа Sum</returns>
        public static Sum operator -(CommonF left, CommonF right)
        {
            //очень простое выражение
            return left + (-right);
        }

        /// <summary>
        /// Оператор отрицания
        /// </summary>
        /// <param name="operand">операнд</param>
        /// <returns>объект типа Sum с одним слагаемым (либо меняет знаки в операнде, если операнд - Sum)</returns>
        public static Sum operator -(CommonF operand)
        {
            Sum result = new Sum();
            if (operand is Sum)
            {
                result = (Sum)operand;
                for (int i = 0; i < result.signs.Count; i++)
                {
                    result.signs[i] = !result.signs[i];
                }
            }
            else
            {
                result.operands.Add(operand);
                result.signs.Add(false);
            }
            return result;
        }

        /// <summary>
        /// Оператор умножения
        /// </summary>
        /// <param name="left">левый операнд</param>
        /// <param name="right">правый операнд</param>
        /// <returns>объект типа Mul</returns>
        public static Mul operator *(CommonF left, CommonF right)
        {
            Mul result = new Mul();
            if (left is Mul)
            {
                result.operands = ((Mul)left).operands;
                result.powers = ((Mul)left).powers;
            }
            else
            {
                result.operands.Add(left);
                result.powers.Add(true);
            }
            if (right is Mul)
            {
                result.operands.AddRange(((Mul)right).operands);
                result.powers.AddRange(((Mul)right).powers);
            }
            else
            {
                result.operands.Add(right);
                result.powers.Add(true);
            }
            return result;
        }

        /// <summary>
        /// Оператор деления
        /// </summary>
        /// <param name="left">левый операнд</param>
        /// <param name="right">правый операнд</param>
        /// <returns>объект типа Mul</returns>
        public static Mul operator /(CommonF left, CommonF right)
        {
            Mul result = new Mul();
            if (left is Mul)
            {
                result.operands = ((Mul)left).operands;
                result.powers = ((Mul)left).powers;
            }
            else
            {
                result.operands.Add(left);
                result.powers.Add(true);
            }

            result.operands.Add(right);
            result.powers.Add(false);
            return result;
        }

        /// <summary>
        /// Оператор возведения в степень
        /// </summary>
        /// <param name="base">основание степени</param>
        /// <param name="power">степнь</param>
        /// <returns>новый объект типа Pow</returns>
        public static Pow operator ^(CommonF @base, CommonF power)
        {
            Pow result = new Pow();
            result.RaiseTo(@base);
            result.RaiseTo(power);
            return result;
        }

        /// <summary>
        /// Главный делегат функции
        /// </summary>
        public abstract Func<double[], double> Invoke { get; }

        /// <summary>
        /// Поиск переменной по имени
        /// </summary>
        /// <param name="name">имя меременной (включая знак ' для производных)</param>
        /// <returns>объект, представляющий переменную, тип Variable</returns>
        public abstract Variable Search(string name);

        /// <summary>
        /// Реализация интерфейса
        /// </summary>
        public Func<double[], double[]> InvokeVec
        {
            get
            {
                return p => { arrayForResult[0] = Invoke(p); return arrayForResult; };
            }
        }

        //оптимизация для того, чтобы не создавать каждый раз новый объект
        private double[] arrayForResult = new double[1];

        /// <summary>
        /// Проверка на то, является ли функция тождественным нулём
        /// </summary>
        /// <returns>логическое значение да или нет</returns>
        public abstract bool IsZero();

        public abstract CommonF Clone();

        /// <summary>
        /// Напечатать функцию в формате языка C
        /// </summary>
        /// <returns>строка, содержащая выражение функции</returns>
        public abstract string Print();

        /// <summary>
        /// Напечатать функцию в формате Latex
        /// </summary>
        /// <returns></returns>
        public abstract string PrintLatex();

        /// <summary>
        /// Преобразование строки в функцию.
        /// </summary>
        /// <param name="s">Строка должна содержать домен переменных после двоеточия, разделённых точкой с запятой, например: "x+y:x;y"</param>
        public static implicit operator CommonF(string s)
        {
            var indexOfBeginingVariablesDomain = s.IndexOf(':');
            var expression = s.Substring(0, indexOfBeginingVariablesDomain);
            var variableDomain = s.Substring(indexOfBeginingVariablesDomain + 1);
            string[] variables = null;
            if (variableDomain != "null")
                variables = variableDomain.Split(';');
            return Parser.ParseExpressionObject(expression, variables);
        }

        public static implicit operator CommonF(double d)
        {
            return new Constant(d);
        }
    }
}
