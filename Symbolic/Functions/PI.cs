﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Symbolic.Functions
{
    /// <summary>
    /// Константа PI
    /// </summary>
    public class PI : Constant
    {
        /// <summary>
        /// Просто конструктор
        /// </summary>
        public PI() : base(Math.PI)
        {
            performance = "PI";
        }
        public override CommonF Clone()
        {
            return new PI();
        }
    }
}