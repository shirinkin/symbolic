﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Symbolic.Functions
{
    /// <summary>
    /// Функция одной переменной
    /// </summary>
    public class OneVar : CommonF
    {
        /// <summary>
        /// аргумент функции - другая функция
        /// </summary>
        public CommonF arg;

        /// <summary>
        /// делегат вычисления функции
        /// </summary>
        public Func<double, double> compute = p => Double.NaN;

        /// <summary>
        /// строковое представление функции
        /// </summary>
        public string performance;

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="function">делегат функции</param>
        /// <param name="arg">аргумент функции</param>
        /// <param name="performance">строковое представление функции, в котором имя переменной заменено словом argument</param>
        public OneVar(Func<double, double> function, CommonF arg, string performance)
        {
            if (performance == "") throw new ArgumentException("empty string of performance");

            this.performance = performance;
            if (arg == null || function == null) throw new ArgumentException("function or argiment is null");
            compute = function;
            this.arg = arg;
            staticDelegate = p => compute(arg.Invoke(p));
        }

        private readonly Func<double[], double> staticDelegate;

        /// <summary>
        /// Главный делегат функции
        /// </summary>
        public override Func<double[], double> Invoke
        {
            get
            {
                return staticDelegate;
            }
        }

        /// <summary>
        /// Проверка на то, является ли функция тождественным нулём
        /// </summary>
        /// <returns>логическое значение да или нет</returns>
        public override bool IsZero()
        {
            return false;
        }

        /// <summary>
        /// Напечатать функцию в формате языка C
        /// </summary>
        /// <returns>строка, содержащая выражение функции</returns>
        public override string Print()
        {
            string result = performance;
            int indexOfArgument = result.IndexOf("argument");
            result = result.Remove(indexOfArgument, 8);
            if (result[indexOfArgument - 1] == '(' && result[indexOfArgument] == ')')
                result = result.Insert(indexOfArgument, arg.Print());
            else
                result = result.Insert(indexOfArgument, "(" + arg.Print() + ")");
            return result;
        }

        public override string PrintLatex()
        {
            int indexOfArgument = performance.IndexOf("argument");
            var name = performance.Substring(0, indexOfArgument - 1);
            var afterName = $"\\left({arg.PrintLatex()}\\right)";
            if (name == "abs")
            {
                return $"\\left|{arg.PrintLatex()}\\right|";
            }
            else if (name == "acos")
            {
                return $"";
            }
            else if (name == "acosh")
            {
                return $"";
            }
            else if (name == "asin")
            {
                return $"";
            }
            else if (name == "asinh")
            {
                return $"";
            }
            else if (name == "atan")
            {
                return $"";
            }
            else if (name == "atanh")
            {
                return $"";
            }
            else if (name == "cbrt")
            {
                return $"";
            }
            else if (name == "ceil")
            {
                return $"";
            }
            else if (name == "cos")
            {
                return $"\\cos{afterName}";
            }
            else if (name == "cosh")
            {
                return $"";
            }
            else if (name == "exp")
            {
                return $"";
            }
            else if (name == "floor")
            {
                return $"";
            }
            else if (name == "log")
            {
                return $"";
            }
            else if (name == "log10")
            {
                return $"";
            }
            else if (name == "signum")
            {
                return $"";
            }
            else if (name == "sin")
            {
                return $"\\sin{afterName}";
            }
            else if (name == "sinh")
            {
                return $"";
            }
            else if (name == "sqrt")
            {
                return $"";
            }
            else if (name == "tan")
            {
                return $"";
            }
            else if (name == "tanh")
            {
                return $"";
            }



            /*
            result = result.Remove(indexOfArgument, 8);
            if (result[indexOfArgument - 1] == '(' && result[indexOfArgument] == ')')
                result = result.Insert(indexOfArgument, "\\right").Insert(indexOfArgument, arg.PrintLatex()).Insert(indexOfArgument - 1, "\\left");
            else
                result = result.Insert(indexOfArgument, "\\left(" + arg.PrintLatex() + "\\right)");

            var name = performance.Substring(0, indexOfArgument - 1);
            performance.Remove(0, indexOfArgument - 1);

            if(name=="abs")
            */
            return "nothing";
        }

        /// <summary>
        /// Поиск переменной по имени
        /// </summary>
        /// <param name="name">имя меременной (включая знак ' для производных)</param>
        /// <returns>объект, представляющий переменную, тип Variable</returns>
        public override Variable Search(string name)
        {
            return arg.Search(name);
        }

        public override CommonF Clone()
        {
            return new OneVar(compute, arg.Clone(), performance);
        }
    }
}
