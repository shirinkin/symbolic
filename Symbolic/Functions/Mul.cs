﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Symbolic.Functions
{
    /// <summary>
    /// Произведение или деление других функций
    /// </summary>
    public class Mul : CommonF
    {
        /// <summary>
        /// список операторов
        /// </summary>
        public List<CommonF> operands;
        /// <summary>
        /// список степеней операторов, входящих в произведение. Степени могут быть +1 или -1. Булево значение показывает это.
        /// </summary>
        public List<bool> powers;

        /// <summary>
        /// Конструктор
        /// </summary>
        public Mul()
        {
            operands = new List<CommonF>();
            powers = new List<bool>();
        }

        /// <summary>
        /// Конструктор с исходными данными
        /// </summary>
        /// <param name="operands">список операндов</param>
        /// <param name="powers">список степеней +1 или -1</param>
        public Mul(List<CommonF> operands, List<bool> powers)
        {
            this.operands = operands;
            this.powers = powers;
        }

        //вычислить произведение при заданных значениях переменных
        private double Compute(double[] p)
        {
            double result = operands[0].Invoke(p);
            for (int i = 1; i < operands.Count; i++)
            {
                if (powers[i])
                {
                    result *= operands[i].Invoke(p);
                }
                else
                {
                    result /= operands[i].Invoke(p);
                }
            }
            return result;
        }

        /// <summary>
        /// Главный делегат функции
        /// </summary>
        public override Func<double[], double> Invoke
        {
            get
            {
                return Compute;
            }
        }

        /// <summary>
        /// Поиск переменной по имени
        /// </summary>
        /// <param name="name">имя меременной (включая знак ' для производных)</param>
        /// <returns>объект, представляющий переменную, тип Variable</returns>
        public override Variable Search(string name)
        {
            Variable result = null;
            for (int i = 0; i < operands.Count; i++)
            {
                if (result == null)
                {
                    result = operands.ElementAt(i).Search(name);
                }
                else
                {
                    return result;
                }
            }
            return result;
        }

        /// <summary>
        /// Проверка на то, является ли функция тождественным нулём
        /// </summary>
        /// <returns>логическое значение да или нет</returns>
        public override bool IsZero()
        {
            if (operands.Count == 0) return true;
            for (int i = 0; i < operands.Count; i++)
            {
                if (operands[i].IsZero()) return true;
            }
            return false;
        }


        /// <summary>
        /// Напечатать функцию в формате языка C
        /// </summary>
        /// <returns>строка, содержащая выражение функции</returns>
        public override string Print()
        {
            string result = "";
            for (int i = 0; i < operands.Count; i++)
            {
                if (i != 0) result += powers[i] ? "*" : "/";
                if (operands[i] is Sum)
                {
                    result += "(" + operands[i].Print() + ")";
                }
                else
                {
                    string factor = operands[i].Print();
                    if (factor[0] != '-')
                        result += factor;
                    else
                        result += "(" + factor + ")";
                }
            }
            return result;
        }

        public override string PrintLatex()
        {
            string result = "";
            for (int i = 0; i < operands.Count; i++)
            {
                if (i != 0) result += powers[i] ? "\\cdot " : "/";
                if (operands[i] is Sum)
                {
                    result += "\\left(" + operands[i].PrintLatex() + "\\right)";
                }
                else
                {
                    string factor = operands[i].PrintLatex();
                    if (factor[0] != '-')
                        result += factor;
                    else
                        result += "\\left(" + factor + "\\right)";
                }
            }
            return result;
        }

        public override CommonF Clone()
        {
            var newOperands = new List<CommonF>();
            var newPowers = new List<bool>();
            for (int i = 0; i < operands.Count; i++)
            {
                newOperands.Add(operands[i].Clone());
                newPowers.Add(powers[i]);
            }
            return new Mul(newOperands, newPowers);
        }
    }
}
