﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Symbolic.Functions
{
    //странный ненужный класс
    public class Curve : SVector
    {
        public Curve(List<CommonF> trajectory) : base(trajectory) { }

        public Func<double, double[]> Trajectory
        {
            get
            {
                return t => Func(new double[] { t });
            }
        }
    }
}
