﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Symbolic.Functions
{
    /// <summary>
    /// Определенная не текстовым выражением функция многих переменных. Функция не имеет аналитического представления.
    /// </summary>
    public class DefinedCommonF : CommonF
    {
        protected Func<double[], double> function;
        protected string performance;

        /// <summary>
        /// Конмтруктор
        /// </summary>
        /// <param name="function">делегат функции</param>
        /// <param name="performance">строковое представление функции</param>
        public DefinedCommonF(Func<double[], double> function, string performance)
        {
            this.performance = performance;
            this.function = function;
        }

        /// <summary>
        /// Главный делегат функции
        /// </summary>
        public override Func<double[], double> Invoke
        {
            get
            {
                return function;
            }
        }

        public override CommonF Clone()
        {
            return new DefinedCommonF(function, performance);
        }

        /// <summary>
        /// Проверка на то, является ли функция тождественным нулём
        /// </summary>
        /// <returns>логическое значение да или нет</returns>
        public override bool IsZero()
        {
            //считаем, что пользователю хватило ума запихнуть в делегат не тождественный ноль
            return false;
        }

        /// <summary>
        /// Напечатать функцию в формате языка C. Выводит заданный пользователем вид функции
        /// </summary>
        /// <returns>строка, содержащая выражение функции</returns>
        public override string Print()
        {
            return performance;
        }

        public override string PrintLatex()
        {
            return performance;
        }

        /// <summary>
        /// Поиск переменной по имени
        /// </summary>
        /// <param name="name">имя меременной (включая знак ' для производных)</param>
        /// <returns>всегда возвращает нуль, потому что функция не имеет аналитического представления</returns>
        public override Variable Search(string name)
        {
            return null;
        }
    }
}
