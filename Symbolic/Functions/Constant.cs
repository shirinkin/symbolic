﻿using System;
using System.Globalization;

namespace Symbolic.Functions
{
    /// <summary>
    /// Числовая константа
    /// </summary>
    public class Constant : OneVar
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="value">значение числовой константы</param>
        public Constant(double value) : base(_ => value, new Variable(-1, "x"), value >= 0 ? "" + value.ToString(CultureInfo.InvariantCulture) : "(" + value.ToString(CultureInfo.InvariantCulture) + ")")
        {
            this.value = value;
        }

        /// <summary>
        /// Напечатать константу в формате языка C
        /// </summary>
        /// <returns>строка, содержащая выражение функции</returns>
        public override string Print()
        {
            return performance;
        }

        public override string PrintLatex()
        {
            if (value == Math.PI || this is PI)
            {
                return "\\pi";
            }
            if (value == Math.E || this is E)
            {
                return "e";
            }
            return Value.ToString("F", CultureInfo.InvariantCulture);
        }

        //значение константы
        private double value;

        /// <summary>
        /// Числовое значение константы
        /// </summary>
        public double Value
        {
            private set
            {
                this.value = value;
                performance = value >= 0 ? "" + value.ToString(CultureInfo.InvariantCulture) : "(" + value.ToString(CultureInfo.InvariantCulture) + ")";
                compute = _ => value;
            }
            get { return value; }
        }

        public override CommonF Clone()
        {
            return new Constant(value);
        }

        /// <summary>
        /// Проверка на то, является ли константа нулём
        /// </summary>
        /// <returns>логическое значение да или нет</returns>
        public override bool IsZero()
        {
            return Value == 0;
        }

        /// <summary>
        /// Переопределённый для константы оператор сложения
        /// </summary>
        /// <param name="left">левый операнд</param>
        /// <param name="right">правый операнд</param>
        /// <returns></returns>
        public static Constant operator +(Constant left, Constant right)
        {
            return new Constant(left.Value + right.Value);
        }
        /// <summary>
        /// Переопределённый для константы оператор вычитания
        /// </summary>
        /// <param name="left">левый операнд</param>
        /// <param name="right">правый операнд</param>
        /// <returns></returns>
        public static Constant operator -(Constant left, Constant right)
        {
            return new Constant(left.Value - right.Value);
        }
        /// <summary>
        /// Переопределённый для константы оператор деления
        /// </summary>
        /// <param name="left">левый операнд</param>
        /// <param name="right">правый операнд</param>
        /// <returns></returns>
        public static Constant operator /(Constant left, Constant right)
        {
            return new Constant(left.Value / right.Value);
        }
        /// <summary>
        /// Переопределённый для константы оператор умножения
        /// </summary>
        /// <param name="left">левый операнд</param>
        /// <param name="right">правый операнд</param>
        /// <returns></returns>
        public static Constant operator *(Constant left, Constant right)
        {
            return new Constant(left.Value * right.Value);
        }
        /// <summary>
        /// Переопределённый для константы оператор отрицания
        /// </summary>
        /// <param name="operand">операнд</param>
        /// <returns></returns>
        public static Constant operator -(Constant operand)
        {
            return new Constant(-operand.value);
        }
        /// <summary>
        /// Переопределённый для константы оператор возведения в степень
        /// </summary>
        /// <param name="base">основание степени</param>
        /// <param name="power">степень</param>
        /// <returns></returns>
        public static Constant operator ^(Constant @base, Constant power)
        {
            return new Constant(Math.Pow(@base.value, power.value));
        }

        public static implicit operator Constant(double c)
        {
            return new Constant(c);
        }
    }
}