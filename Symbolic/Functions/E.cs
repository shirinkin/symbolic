﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Symbolic.Functions
{
    /// <summary>
    /// Константа E
    /// </summary>
    public class E : Constant
    {
        /// <summary>
        /// Просто конструктор
        /// </summary>
        public E() : base(Math.E)
        {
            performance = "E";
        }
        public override CommonF Clone()
        {
            return new E();
        }
    }
}