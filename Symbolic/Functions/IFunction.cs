﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Symbolic.Functions
{
    /// <summary>
    /// Главный интерфейс. В текущей версии библиотеки почти не используется.
    /// </summary>
    public interface IFunction
    {
        /// <summary>
        /// Главный делегат вектор-функции многих переменных
        /// </summary>
        Func<double[], double[]> InvokeVec { get; }

        /// <summary>
        /// Поиск переменной по имени
        /// </summary>
        /// <param name="name">имя меременной (включая знак ' для производных)</param>
        /// <returns>объект, представляющий переменную, тип Variable</returns>
        Variable Search(string name);
    }
}
