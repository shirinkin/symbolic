﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Symbolic.Functions
{
    /// <summary>
    /// Переменная
    /// </summary>
    public class Variable : CommonF
    {
        /// <summary>
        /// номер переменной в порядке её появления в выражении
        /// </summary>
        public int index;
        /// <summary>
        /// имя переменной. может содержать ' для обозначения производной
        /// </summary>
        public string name;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="index">номер переменной в порядке её появления в выражении</param>
        /// <param name="name">имя переменной. может содержать ' для обозначения производной</param>
        public Variable(int index, string name)
        {
            this.index = index;
            this.name = name;
            thisValueDelegate = p => p[index];
        }

        //для оптимизации
        private readonly Func<double[], double> zeroDelegate = x => 0;
        private readonly Func<double[], double> thisValueDelegate;

        /// <summary>
        /// Главный делегат функции
        /// </summary>
        public override Func<double[], double> Invoke
        {
            get
            {
                if (index == -1) return zeroDelegate;

                return thisValueDelegate;
            }
        }

        /// <summary>
        /// Проверка на то, является ли функция тождественным нулём
        /// </summary>
        /// <returns>логическое значение да или нет</returns>
        public override bool IsZero()
        {
            return false;
        }

        /// <summary>
        /// Напечатать переменную в формате языка C
        /// </summary>
        /// <returns>строка, содержащая выражение функции</returns>
        public override string Print()
        {
            return name;
        }

        public override string PrintLatex()
        {
            var result = name + "";
            var indexofPhi = result.IndexOf("phi");
            if (indexofPhi != -1)
            {
                result = result.Remove(indexofPhi, 3);
                result=result.Insert(indexofPhi, "\\varphi");

                if (result.IndexOf("'''") == -1)
                    if (result.IndexOf("''") == result.Length - 2)
                    {
                        result = result.Remove(result.Length - 2, 2);
                        //{\ddot{
                        //\varphi
                        //}}
                        result = result.Insert(indexofPhi + 7, "}}");
                        result = result.Insert(indexofPhi, "{\\ddot{");
                    }
                    else if (result.IndexOf("'") == result.Length - 1)
                    {
                        result = result.Remove(result.Length - 1, 1);
                        result = result.Insert(indexofPhi + 7, "}}");
                        result = result.Insert(indexofPhi, "{\\dot{");
                    }
            }
            return result;
        }

        /// <summary>
        /// Поиск переменной по имени
        /// </summary>
        /// <param name="name">имя меременной (включая знак ' для производных)</param>
        /// <returns>объект, представляющий переменную, тип Variable</returns>
        public override Variable Search(string name)
        {
            if (this.name == name)
            {
                return this;
            }
            else
            {
                return null;
            }
        }

        public override CommonF Clone()
        {
            return new Variable(index, name);
        }
    }
}
