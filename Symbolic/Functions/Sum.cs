﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Symbolic.Functions
{
    /// <summary>
    /// Сумма других функций
    /// </summary>
    public class Sum : CommonF
    {
        /// <summary>
        /// операнды слагаемые
        /// </summary>
        public List<CommonF> operands;
        /// <summary>
        /// знак + или - для каждого операнда
        /// </summary>
        public List<bool> signs;

        /// <summary>
        /// конструктор
        /// </summary>
        public Sum()
        {
            operands = new List<CommonF>();
            signs = new List<bool>();
        }

        /// <summary>
        /// конструктор с исходными данными
        /// </summary>
        /// <param name="operands">операнды</param>
        /// <param name="signs">знаки + - для каждого операнда</param>
        public Sum(List<CommonF> operands, List<bool> signs)
        {
            this.operands = operands;
            this.signs = signs;
        }

        /// <summary>
        /// вычислить сумму
        /// </summary>
        /// <param name="p">массив переменных, от которых зависят функции в порядке их появления в выражении</param>
        /// <returns>значение суммы</returns>
        private double Compute(double[] p)
        {
            double result = 0;
            for (int i = 0; i < operands.Count; i++)
            {
                if (signs[i])
                {
                    result += operands[i].Invoke(p);
                }
                else
                {
                    result -= operands[i].Invoke(p);
                }
            }
            return result;
        }
        /// <summary>
        /// Главный делегат функции
        /// </summary>
        public override Func<double[], double> Invoke
        {
            get
            {
                return Compute;
            }
        }
        /// <summary>
        /// Поиск переменной по имени
        /// </summary>
        /// <param name="name">имя меременной (включая знак ' для производных)</param>
        /// <returns>объект, представляющий переменную, тип Variable</returns>
        public override Variable Search(string name)
        {
            Variable result = null;

            for (int i = 0; i < operands.Count; i++)
            {
                if (result == null)
                {
                    result = operands.ElementAt(i).Search(name);
                }
                else
                {
                    return result;
                }
            }
            return result;
        }
        /// <summary>
        /// Проверка на то, является ли функция тождественным нулём
        /// </summary>
        /// <returns>логическое значение да или нет</returns>
        public override bool IsZero()
        {
            if (operands.Count == 0) return true;
            else return operands.All(it => it.IsZero());
        }
        /// <summary>
        /// Напечатать функцию в формате языка C
        /// </summary>
        /// <returns>строка, содержащая выражение функции</returns>
        public override string Print()
        {
            string result = "";
            for (int i = 0; i < operands.Count; i++)
            {
                if (i == 0)
                {
                    if (!signs[0])
                    {
                        result += "-";
                    }
                }
                else
                {
                    result += signs[i] ? "+" : "-";
                }
                if (!signs[i])
                {
                    if (operands[i] is Sum)
                    {
                        result += "(" + operands[i].Print() + ")";
                    }
                    else
                    {
                        string term = operands[i].Print();
                        if (term[0] != '-')
                            result += term;
                        else
                            result += "(" + term + ")";
                    }
                }
                else
                {
                    string term = operands[i].Print();
                    if (term[0] != '-')
                        result += term;
                    else
                        result += "(" + term + ")";
                }
            }
            return result;
        }

        public override string PrintLatex()
        {
            string result = "";
            for (int i = 0; i < operands.Count; i++)
            {
                if (i == 0)
                {
                    if (!signs[0])
                    {
                        result += "-";
                    }
                }
                else
                {
                    result += signs[i] ? "+" : "-";
                }
                if (!signs[i])
                {
                    if (operands[i] is Sum)
                    {
                        result += "\\left(" + operands[i].PrintLatex() + "\\right)";
                    }
                    else
                    {
                        string term = operands[i].PrintLatex();
                        if (term[0] != '-')
                            result += term;
                        else
                            result += "\\left(" + term + "\\right)";
                    }
                }
                else
                {
                    string term = operands[i].PrintLatex();
                    if (term[0] != '-')
                        result += term;
                    else
                        result += "\\left(" + term + "\\right)";
                }
            }
            return result;
        }
        public override CommonF Clone()
        {
            var newOperands = new List<CommonF>();
            var newSigns = new List<bool>();
            for (int i = 0; i < operands.Count; i++)
            {
                newOperands.Add(operands[i].Clone());
                newSigns.Add(signs[i]);
            }
            return new Sum(newOperands, newSigns);
        }
    }
}
