﻿using Symbolic;
using Symbolic.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Symbolic.Functions.StaticInput;

namespace TestMatrix
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var f11 = Parser.ParseExpressionObject(box_11.Text, null);
            var f12 = Parser.ParseExpressionObject(box_12.Text, null);
            var f13 = Parser.ParseExpressionObject(box_13.Text, null);
            var f21 = Parser.ParseExpressionObject(box_21.Text, null);
            var f22 = Parser.ParseExpressionObject(box_22.Text, null);
            var f23 = Parser.ParseExpressionObject(box_23.Text, null);
            var f31 = Parser.ParseExpressionObject(box_31.Text, null);
            var f32 = Parser.ParseExpressionObject(box_32.Text, null);
            var f33 = Parser.ParseExpressionObject(box_33.Text, null);

            Symbolic.Functions.Matrix m = new CommonF[,] { { f11, f12, f13 }, { f21, f22, f23 }, { f31, f32, f33 } };
            var determinant = m.Determinant;
            label_result.Content = Parser.Optimize(determinant.Print());

            setVariableDomain("x;y;z");
            CommonF f = _sin(_E) * _log(_log10(_var("x"))) + _pow(_var("y"), _var("z"));
            var fS = f.Print();
        }
    }
}
